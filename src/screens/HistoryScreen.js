import React, {useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {Subheading, Surface} from 'react-native-paper';
import Background from '../components/Background';
import {theme} from '../core/theme';

const HistoryScreen = ({navigation}) => {
  const [data, setData] = useState([
    {
      id: 1,
      name: 'Toyota 86 GT',
      type: 'Rented',
      from: '1.1.2021',
      to: '5.1.2021',
      image:
        'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
    },
    {
      id: 2,
      name: 'Toyota 86 GT',
      type: 'Rented',
      from: '1.12.2020',
      to: '5.12.2020',
      image:
        'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
    },
    {
      id: 3,
      name: 'Toyota 86 GT',
      type: 'Leased',
      from: '1.9.2020',
      to: '14.9.2020',
      image:
        'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
    },
    {
      id: 4,
      name: 'Toyota 86 GT',
      type: 'Leased',
      from: '25.7.2020',
      to: '5.8.2020',
      image:
        'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
    },
    {
      id: 5,
      name: 'Toyota 86 GT',
      type: 'Leased',
      from: '7.7.2020',
      to: '10.7.2020',
      image:
        'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
    },
    {
      id: 6,
      name: 'Toyota 86 GT',
      type: 'Leased',
      from: '5.5.2020',
      to: '5.6.2020',
      image:
        'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
    },
    {
      id: 7,
      name: 'Toyota 86 GT',
      type: 'Leased',
      from: '1.1.2020',
      to: '5.1.2020',
      image:
        'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
    },
    {
      id: 8,
      name: 'Toyota 86 GT',
      type: 'Leased',
      from: '1.4.2020',
      to: '15.4.2020',
      image:
        'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
    },
    {
      id: 9,
      name: 'Toyota 86 GT',
      type: 'Leased',
      from: '14.2.2020',
      to: '17.2.2020',
      image:
        'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
    },
    {
      id: 10,
      name: 'Toyota 86 GT',
      type: 'Leased',
      from: '1.1.2020',
      to: '5.1.2020',
      image:
        'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
    },
  ]);

  return (
    <Background>
      <FlatList
        style={styles.userList}
        columnWrapperStyle={styles.listContainer}
        data={data}
        keyExtractor={(item) => {
          return item.id;
        }}
        renderItem={({item}) => {
          return (
            <Surface>
              <TouchableOpacity
                style={styles.card}
                onPress={() => {
                  navigation.navigate('PostView');
                }}>
                <Image style={styles.image} source={{uri: item.image}} />
                <View style={styles.cardContent}>
                  <View style={styles.cardColumn}>
                    <Subheading style={styles.name}>{item.name}</Subheading>
                    <Subheading style={styles.type}>{item.type}</Subheading>
                  </View>
                  <View style={styles.cardColumn}>
                    <Subheading>{`From: ${item.to}`}</Subheading>
                    <Subheading>{`     To: ${item.from}`}</Subheading>
                  </View>
                </View>
              </TouchableOpacity>
            </Surface>
          );
        }}
      />
    </Background>
  );
};

const styles = StyleSheet.create({
  userList: {
    flex: 1,
    marginTop: 45,
    width: '100%',
  },
  cardContent: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cardColumn: {
    flexDirection: 'column',
    marginLeft: 20,
  },
  image: {
    width: 90,
    height: '100%',
  },

  card: {
    elevation: 6,
    marginVertical: 8,
    marginHorizontal: 20,
    backgroundColor: 'white',
    flexDirection: 'row',
  },

  name: {
    // color: '#008080',
    fontWeight: 'bold',
  },
  type: {
    color: theme.colors.primary,
  },
  about: {
    marginHorizontal: 10,
  },
});

export default HistoryScreen;
