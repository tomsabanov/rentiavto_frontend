import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {
  Avatar,
  Button,
  Caption,
  Card,
  Chip,
  IconButton,
  Paragraph,
  Text,
  Title,
} from 'react-native-paper';
import Background from '../components/Background';
import {AirbnbRating} from 'react-native-ratings';
import {theme} from '../core/theme';

const ProfileViewScreen = ({navigation}) => {
  // const mockUser = ;

  const [user, setUser] = useState({
    avatar:
      'https://pbs.twimg.com/profile_images/952545910990495744/b59hSXUd_400x400.jpg',
    name: 'Dawid Urbaniak',
    handle: '@trensik',
    bio: 'Short user bio example',
  });

  return (
    <Background>
      <View style={styles.container}>
        <View style={styles.userRow}>
          {console.log(user)}
          <Avatar.Image
            source={{
              uri: user.avatar,
            }}
            size={150}
          />
          <View style={styles.userNameRow}>
            <Title
              // style={styles.title}
              style={styles.userNameText}>
              {user.name}
            </Title>
            <Caption style={styles.caption}>{user.handle}</Caption>
          </View>
          <View style={styles.row}>
            <Text style={styles.userBioText}>{user.bio}</Text>
          </View>

          <View style={styles.row}>
            <View style={styles.section}>
              <Paragraph style={[styles.paragraph, styles.caption]}>
                202
              </Paragraph>
              <Caption style={styles.caption}>Following</Caption>
            </View>
            <View style={styles.section}>
              <Paragraph style={[styles.paragraph, styles.caption]}>
                159
              </Paragraph>
              <Caption style={styles.caption}>Followers</Caption>
            </View>
          </View>

          <View style={styles.starContainer}>
            <AirbnbRating showRating={false} isDisabled={true} size={30} />
          </View>

          <View style={styles.row}>
            <View style={styles.infoChip}>
              <Text style={styles.infoText}>25</Text>
              <Text>Listings</Text>
            </View>
            <View style={styles.infoChip}>
              <Text style={styles.infoText}>5</Text>
              <Text>Rentals</Text>
            </View>
            <View style={styles.infoChip}>
              <Text style={styles.infoText}>15</Text>
              <Text>Leases</Text>
            </View>
          </View>
        </View>

        <View style={styles.walletRow}>
          <View style={styles.walletWrapper}>
            {/*Needs wrapper to fit size to content*/}
            <Card style={styles.wallet}>
              <Card.Title
                title="Wallet"
                left={() => (
                  <IconButton
                    icon="credit-card-outline"
                    // color={color}
                    // size={size}
                  />
                )}
              />
              <Card.Content style={styles.walletMargin}>
                <Paragraph>Balance</Paragraph>
                <Title>500.00€</Title>
              </Card.Content>
              {/*<Card.Cover source={{ uri: 'https://picsum.photos/700' }} />*/}
              <Card.Actions
                // style={{justifyContent: 'flex-end'}}
                style={styles.walletMargin}>
                <Button>Deposit</Button>
                <Button>Withdraw</Button>
              </Card.Actions>
            </Card>
          </View>
        </View>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 45,
    flex: 1,
  },
  userRow: {
    marginTop: 20,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  userNameRow: {
    marginTop: 10,
    alignItems: 'center',
  },
  userNameText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  userBioText: {
    color: 'gray',
    fontSize: 16,
    marginBottom: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    textAlign: 'center',
    fontSize: 16,
  },
  infoChip: {
    flexDirection: 'column',
    marginHorizontal: 20,
    alignItems: 'center',
  },
  infoText: {
    fontWeight: 'bold',
    fontSize: 22,
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  paragraph: {
    fontWeight: 'bold',
  },
  starContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 20,
  },

  walletRow: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  walletWrapper: {
    justifyContent: 'flex-end',
  },
  wallet: {
    width: 350,
    backgroundColor: '#C4FCF0',
    marginBottom: 10,
  },
  walletMargin: {
    marginHorizontal: 10,
  },
});

export default ProfileViewScreen;
