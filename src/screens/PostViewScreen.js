import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {
  Caption,
  FAB,
  Headline,
  Paragraph,
  Surface,
  Text,
} from 'react-native-paper';
import Swiper from 'react-native-swiper';
import StatusBarMarginContainer from '../components/StatusBarMarginContainer';
import {theme} from '../core/theme';
import Spinner from 'react-native-loading-spinner-overlay';

const PostViewScreen = ({navigation}, props) => {
  const icons = {
    seat: 'https://i.ibb.co/M27HkLw/seaticon.png',
    door: 'https://i.ibb.co/C5j1PLT/doorIcon.png',
    engine: 'https://i.ibb.co/Xktfn52/engine.png',
    transmission: 'https://i.ibb.co/V2RSC7b/transmission-Icon.png',
    drivetrain: 'https://i.ibb.co/nQgfRv9/drivetrain-Icon.png',
  };

  const [post, setPost] = useState({});

  useEffect(() => {
    //TODO fetch post for props.postId
    //use mock for now
    setPost(mockPost);
  }, [props.postId]);

  let imageList = mockImages.map((img, id) => {
    return (
      <View key={id} style={styles.slide}>
        <Image source={{uri: img}} style={styles.slide} />
      </View>
    );
  });

  return (
    <StatusBarMarginContainer>
      {post ? (
        <Surface style={styles.surface}>
          <View style={styles.swiperContainer}>
            <Swiper showsButtons={false}>{imageList}</Swiper>
          </View>

          <View style={styles.contentContainer}>
            <View style={styles.flexRow}>
              <View>
                <Text
                  style={
                    styles.header
                  }>{`${post.carBrand} ${post.carModel}`}</Text>
                <Paragraph>{`${post.carMake}, ${post.carTransmission} transmission`}</Paragraph>
                <Paragraph>{`${post.carEngine} ${post.carFuelType} engine, ${post.carDrivetrain}`}</Paragraph>
                {/*<Subheading>Additional details</Subheading>*/}
              </View>

              <View style={{alignSelf: 'center'}}>
                <FAB
                  label="Book"
                  small
                  onPress={() => navigation.navigate('Dashboard')}
                />
              </View>
            </View>
          </View>

          <View style={styles.infoIcons}>
            <View>
              <Image
                source={{
                  uri: icons.seat,
                }}
                style={styles.icon}
                resizeMode="contain"
              />
              <Paragraph
                style={styles.iconLabel}>{`${post.carSeats} seats`}</Paragraph>
            </View>
            <View>
              <Image
                source={{
                  uri: icons.door,
                }}
                style={styles.icon}
                resizeMode="contain"
              />
              <Paragraph
                style={styles.iconLabel}>{`${post.carDoors} doors`}</Paragraph>
            </View>

            <View>
              <Image
                source={{
                  uri: icons.transmission,
                }}
                style={styles.icon}
                resizeMode="contain"
              />
              <Paragraph style={styles.iconLabel}>
                {post.carTransmission}
              </Paragraph>
            </View>
          </View>

          <View style={styles.priceContainer}>
            <View>
              <View style={styles.flexRow}>
                <Headline>{`${post.kmIncluded} `}</Headline>
                <Paragraph>kilometers included</Paragraph>
              </View>
              <Caption
                style={
                  styles.caption
                }>{`${post.priceAddKm}€ per additional kilometer`}</Caption>
            </View>

            {/*<Button
              style={styles.bookButton}
              //TODO button onPress implementation
              mode="contained"
              onPress={() => console.log('Book pressed')}>
              Book
            </Button>*/}
            <Text style={styles.price}>
              {post.pricePerDay + ' €\n'}
              <Paragraph>per day</Paragraph>
            </Text>
          </View>

          <View style={styles.locationContainer}>
            <Paragraph>{`Location: ${post.location}`}</Paragraph>
          </View>

          <View style={styles.mapContainer}>
            <Image
              // TODO replace with map component
              source={{
                uri:
                  'https://9to5mac.com/wp-content/uploads/sites/6/2020/08/Screenshot-2020-08-12-at-11.53.31.png',
              }}
              style={styles.map}
            />
          </View>
        </Surface>
      ) : (
        <Spinner visible textContent={'Fetching post details'} />
      )}
    </StatusBarMarginContainer>
  );
};

const styles = StyleSheet.create({
  surface: {
    flex: 1,
    margin: 10,
    elevation: 4,
  },
  swiperContainer: {
    height: '30%',
  },
  slide: {
    flex: 1,
    height: '30%',
  },
  contentContainer: {
    // flex: 1,
    padding: 15,
    // alignItems: 'flex-start',
  },
  priceContainer: {
    flex: 1,
    margin: 15,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: 'lightgray',
  },
  locationContainer: {
    marginHorizontal: 15,
    paddingVertical: 15,
    borderTopWidth: 1,
    borderTopColor: 'lightgray',
  },
  bookButton: {
    alignSelf: 'center',
  },
  header: {
    fontSize: 26,
    color: theme.colors.primary,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  price: {
    textAlign: 'center',
    fontSize: 26,
    color: theme.colors.primary,
    fontWeight: 'bold',
  },

  infoIcons: {
    width: '70%',
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  icon: {
    // flex: 1,
    height: 50,
    width: 50,
    marginBottom: 2,
  },
  iconLabel: {},

  mapContainer: {
    alignSelf: 'flex-end',
    alignItems: 'stretch',
    width: '100%',
    height: '25%',
  },
  map: {
    flex: 1,
  },

  flexRow: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  caption: {
    marginLeft: 5,
    marginTop: -5,
    marginBottom: 5,
  },
});

export default PostViewScreen;

const mockImages = [
  'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0778_1000x1000.JPG?v=1502860857',
  'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0781_1000x1000.JPG?v=1502860857',
  'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0780_1000x1000.JPG?v=1502860857',
  'https://cdn.shopify.com/s/files/1/1063/6350/products/IMG_0779_1200x.JPG?v=1502860857',
];

const mockPost = {
  location: '1000, Ljubljana, Slovenia',
  pricePerDay: 250,
  kmIncluded: 100,
  priceAddKm: '1.0',
  carBrand: 'Toyota',
  carModel: '86 GT',
  carMake: '2018',
  carTransmission: 'Manual',
  carSeats: 4,
  carDoors: 2,
  carEngine: 'H4',
  carFuelType: 'Gasoline',
  carDrivetrain: 'RWD',
};
