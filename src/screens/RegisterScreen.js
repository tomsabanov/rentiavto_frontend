import React, {useState} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {Text} from 'react-native-paper';
import Background from '../components/Background';
import Logo from '../components/Logo';
import Header from '../components/Header';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import BackButton from '../components/BackButton';

import {theme, config} from '../core/theme';
import {
  emailValidator,
  passwordValidator,
  nameValidator,
} from '../helpers/validators';
import Spinner from 'react-native-loading-spinner-overlay';

const RegisterScreen = ({navigation}) => {
  const [name, setName] = useState({value: '', error: ''});
  const [surname, setSurname] = useState({value: '', error: ''});
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});
  const [spinner, setSpinner] = useState(false);

  const onSignUpPressed = () => {
    const nameError = nameValidator(name.value);
    const surnameError = nameValidator(surname.value);
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    if (emailError || passwordError || nameError || surnameError) {
      setName({...name, error: nameError});
      setSurname({...surname, error: surnameError});
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      return;
    }

    setSpinner(true);
    // Check registration info

    fetch(config.url + '/register', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: name.value,
        surname: surname.value,
        email: email.value,
        password: password.value,
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        setSpinner(false);

        if (response.error != null) {
          // TODO: Handle error
          return;
        }

        let userId = response.userId;
        navigation.reset({
          index: 0,
          routes: [
            {
              name: 'MainScreen',
              params: {userId: userId},
            },
          ],
        });
      })
      .catch((error) => {
        setSpinner(false);
        console.error(error);
        // TODO: Handle error
      });
  };

  return (
    <Background>
      <Spinner
        visible={spinner}
        textContent={'Checking register info'}
        textStyle={styles.spinnerTextStyle}
      />
      <View style={styles.registerContainer}>
        <BackButton goBack={navigation.goBack} />
        <Logo />
        <Header>Create Account</Header>
        <TextInput
          label="Name"
          returnKeyType="next"
          value={name.value}
          onChangeText={(text) => setName({value: text, error: ''})}
          error={!!name.error}
          errorText={name.error}
        />
        <TextInput
          label="Surname"
          returnKeyType="next"
          value={surname.value}
          onChangeText={(text) => setSurname({value: text, error: ''})}
          error={!!surname.error}
          errorText={surname.error}
        />
        <TextInput
          label="Email"
          returnKeyType="next"
          value={email.value}
          onChangeText={(text) => setEmail({value: text, error: ''})}
          error={!!email.error}
          errorText={email.error}
          autoCapitalize="none"
          autoCompleteType="email"
          textContentType="emailAddress"
          keyboardType="email-address"
        />
        <TextInput
          label="Password"
          returnKeyType="done"
          value={password.value}
          onChangeText={(text) => setPassword({value: text, error: ''})}
          error={!!password.error}
          errorText={password.error}
          secureTextEntry
        />
        <Button
          mode="contained"
          onPress={onSignUpPressed}
          style={{marginTop: 24}}>
          Sign Up
        </Button>
        <View style={styles.row}>
          <Text>Already have an account? </Text>
          <TouchableOpacity onPress={() => navigation.replace('LoginScreen')}>
            <Text style={styles.link}>Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  registerContainer: {
    width: '100%',
    height: '100%',
    maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
});

export default RegisterScreen;
