import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import {IconButton, FAB} from 'react-native-paper';
import Modal from 'react-native-modal';

import Background from '../components/Background';
import Bar from '../components/Bar';
import Grid from '../components/Grid';
import Chips from '../components/Chips';

/*
        <Button
          mode="outlined"
          onPress={() =>
            navigation.reset({
              index: 0,
              routes: [{name: 'StartScreen'}],
            })
          }>
          Logout
        </Button>

  */

const random = () => {
  return Math.floor(Math.random() * 100000) + 1 + '';
};

const PriceFilter = ({visible, setVisible, setOpenState, addFilter}) => {
  const onModalClose = () => {
    setOpenState({open: true});
    setVisible(false);
  };
  const onAddFilter = (filter) => {
    setOpenState({open: true});
    setVisible(false);
    addFilter(filter);
  };

  return (
    <Modal
      isVisible={visible}
      style={styles.modal}
      onBackdropPress={onModalClose}
      onBackButtonPress={onModalClose}>
      <View style={styles.filter}>
        <Text>I am the PRICE MODAL content!</Text>
        <Button
          title="Dodaj filter za ceno"
          onPress={() => onAddFilter({title: 'Filter', id: random()})}
        />
      </View>
    </Modal>
  );
};
const CategoryFilter = ({visible, setVisible, setOpenState, addFilter}) => {
  const onModalClose = () => {
    setOpenState({open: true});
    setVisible(false);
  };
  const onAddFilter = (filter) => {
    setOpenState({open: true});
    setVisible(false);
    addFilter(filter);
  };

  return (
    <Modal
      isVisible={visible}
      style={styles.modal}
      onBackdropPress={onModalClose}
      onBackButtonPress={onModalClose}>
      <View style={styles.filter}>
        <Text>I am the CATEGORY MODAL content!</Text>
        <Button
          title="Add Filter"
          onPress={() => onAddFilter({title: 'Filter', id: random()})}
        />
      </View>
    </Modal>
  );
};

const DashboardScreen = ({navigation}) => {
  // FAB
  const [openState, setState] = React.useState({open: false});
  const onStateChange = ({open}) => setState({open});

  // PriceFilter
  const [isPriceModalVisible, setPriceModalVisible] = React.useState(false);
  // CategoryFilter
  const [isCategoryModalVisible, setCategoryModalVisible] = React.useState(
    false,
  );

  // Filters and add/remove functions
  const [selectedFilters, setSelectedFilters] = React.useState([]);
  const addFilter = (filter) => {
    setSelectedFilters([...selectedFilters, filter]);
  };
  const removeFilter = (id) => {
    let filters = [];
    for (let f of selectedFilters) {
      console.log(f);
      if (f.id == id) {
        continue;
      }
      filters.push(f);
    }
    setSelectedFilters(filters);
  };

  return (
    <View style={styles.container}>
      <PriceFilter
        visible={isPriceModalVisible}
        setVisible={setPriceModalVisible}
        setOpenState={setState}
        addFilter={addFilter}
      />
      <CategoryFilter
        visible={isCategoryModalVisible}
        setVisible={setCategoryModalVisible}
        setOpenState={setState}
        addFilter={addFilter}
      />

      <Bar openDrawer={navigation.openDrawer} />
      <Background>
        <View style={styles.filterBar}>
          <IconButton
            icon="google-maps"
            color={'blue'}
            size={25}
            onPress={() => navigation.navigate('MapView')}
          />
          <Chips filters={selectedFilters} removeFilter={removeFilter} />
        </View>
        <Grid navigation={navigation}/>
        <FAB.Group
          open={openState.open}
          icon={openState.open ? 'filter' : 'filter-outline'}
          actions={[
            {
              icon: 'currency-eur',
              label: 'Cena',
              onPress: () => setPriceModalVisible(true),
            },
            {
              icon: 'car',
              label: 'Kategorije',
              onPress: () => setCategoryModalVisible(true),
            },
          ]}
          onStateChange={onStateChange}
          onPress={() => {
            if (openState.open) {
              // do something if the speed dial is open
            }
          }}
        />
      </Background>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  filterBar: {
    height: '5%',
    width: '100%',
    flexDirection: 'row',
  },
  filter: {
    backgroundColor: 'white',
    height: '50%',
  },
});

export default DashboardScreen;
