import React, {useState, useEffect, Platform} from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
  ScrollView,
  Text,
} from 'react-native';

import Swiper from 'react-native-swiper';
import {
  Appbar,
  Button,
  List,
  IconButton,
  TouchableRipple,
} from 'react-native-paper';
import * as ImagePicker from 'expo-image-picker';
import ImageView from 'react-native-image-view';

import Background from '../components/Background';
import SelectPicker from '../components/SelectPicker';
import {deviceHeight} from '../core/constants';
import {doc} from 'prettier';
import {getNumbers} from '../core/utils';

const mockPost = {
  brand: null,
  model: null,
  year: null,
  transmission: 'Manual',
  bodyType: null,
  seats: '5',
  doors: '4',
  fuelType: 'Gasoline',
  fuelUsage: null,
  registrationNumber: null,
  documents: [],
  images: [],
  //////////////////////////////////////
  pricePerDay: null,
  additionalHour: 5,
  additionalKM: 0.5,
  // limitations
  ageLimit: 18,
  // additional insurance
  insurance: [],
};

let items = [
  {label: 'Ha', value: 'ha'},
  {label: 'Fiat', value: 'da'},
  {label: 'Fiat', value: 'hza'},
];

const cars = require('../assets/cars.json');
const bikes = require('../assets/bikes.json');

function compare(a, b) {
  if (a.label < b.label) {
    return -1;
  }
  if (a.label > b.label) {
    return 1;
  }
  return 0;
}

const getCarBrands = (cars) => {
  let items = [];
  let keys = Object.keys(cars);
  for (let key of keys) {
    let x = {
      label: key,
      value: key,
    };
    items.push(x);
  }
  return items;
};
const getCarModels = (model) => {
  if (model == null) {
    return [];
  }
  let inModels = {};
  let models = cars[model];
  let items = [];
  for (let m of models) {
    if (m.model in inModels) {
      continue;
    }
    let x = {
      label: m.model,
      value: m.model,
    };
    inModels[m.model] = true;
    items.push(x);
  }
  return items.sort(compare);
};
const brands = getCarBrands(cars);

const years = getNumbers(1952, 2021);
const doors = getNumbers(1, 10);
const seats = getNumbers(1, 10);
const fuels = [
  {
    label: 'Bencin',
    value: 'bencin',
  },
  {
    label: 'Dizel',
    value: 'dizel',
  },
];
const transmissions = [
  {
    label: 'Ročni menjalnik',
    value: 'rocni',
  },
  {
    label: 'Avtomatski menjalnik',
    value: 'avtomatski',
  },
];
const averageFuelUsage = getNumbers(1.0, 10.0, 0.1);

const CreatePostScreen = ({navigation}) => {
  // images from uploading and camera preview
  const [images, setImages] = useState([]);
  const [selectedPreviewImageID, setSelectedPreviewImageID] = useState(0);

  // Data of Post
  const [postData, setPostData] = useState(mockPost);
  const [models, setModels] = useState([]);

  //  Selected document
  const [selectedDoc, setSelectedDoc] = useState({
    doc: null,
    visible: false,
  });

  /*
  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const {
          status,
        } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);
  */

  const changeData = (key, value) => {
    console.log(key + ' ' + value);
    setPostData({...postData, key: value});
    if (key == 'brand') {
      setModels(getCarModels(value));
    }
  };

  const removeImage = (img) => {
    let newImages = [];
    for (let im of images) {
      if (im == img) {
        continue;
      }
      newImages.push(im);
    }
    setSelectedPreviewImageID(0); // When image is deleted, previewImageID should be reset as well....
    setImages(newImages);
  };

  const pickImageFromFS = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    if (!result.cancelled) {
      setImages(images.concat(result.uri));
    }
  };
  const pickDocumentFromFS = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      quality: 1,
    });
    if (!result.cancelled) {
      setPostData({
        ...postData,
        documents: postData.documents.concat(result.uri),
      });
    }
  };

  const takeImageFromCamera = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImages(images.concat(result.uri));
    }
  };

  let imageList = images.map((img, id) => {
    let icon = 'checkbox-blank-circle-outline';
    if (id == selectedPreviewImageID) {
      icon = 'checkbox-blank-circle';
    }
    return (
      <ImageBackground source={{uri: img}} style={styles.image}>
        <IconButton
          icon={'close'}
          size={35}
          color="white"
          style={styles.closeImage}
          onPress={() => removeImage(img)}
        />
        <IconButton
          icon={icon}
          size={35}
          color="white"
          style={styles.selectedImagePreview}
          onPress={() => setSelectedPreviewImageID(id)}
        />
      </ImageBackground>
    );
  });
  const defaultImage = (
    <ImageBackground
      source={require('../assets/defaultCar.png')}
      style={styles.image}
    />
  );
  let im = images.length == 0 ? defaultImage : imageList;

  const removeDocument = (id) => {
    let docs = [];
    let i = 0;
    for (let d of postData.documents) {
      if (i == id) {
        i = i + 1;
        continue;
      }
      docs.push(d);
      i = i + 1;
    }
    setPostData({...postData, documents: docs});
  };

  let documents = postData.documents.map((img, id) => {
    let icon = 'checkbox-blank-circle-outline';
    if (id == selectedPreviewImageID) {
      icon = 'checkbox-blank-circle';
    }
    return (
      <List.Item
        title={img}
        onPress={setSelectedDoc({doc: img, visible: true})}
        key={id}
        left={(props) => <List.Icon {...props} icon="folder" />}
        right={(props) => (
          <TouchableRipple onPress={() => removeDocument(id)}>
            <List.Icon {...props} icon="close" />
          </TouchableRipple>
        )}
      />
    );
  });

  return (
    <View style={styles.container}>
      <Appbar.Header style={styles.header} statusBarHeight={30}>
        <Appbar.Action
          icon="arrow-left"
          color="gray"
          size={30}
          onPress={navigation.goBack}
          style={styles.icon}
        />
        <Appbar.Action
          icon="camera"
          color="gray"
          size={30}
          onPress={takeImageFromCamera}
          style={styles.cameraIcon}
        />
        <Appbar.Action
          icon="file"
          color="gray"
          size={30}
          onPress={pickImageFromFS}
          style={styles.photoIcon}
        />
      </Appbar.Header>
      <Background>
        <ScrollView style={styles.scrollContainer}>
          <Swiper showsButtons={false} height={deviceHeight / 3}>
            {im}
          </Swiper>
          <View style={styles.optionsContainer}>
            <SelectPicker
              caption={'Znamka'}
              items={brands}
              objKey={'brand'}
              cb={changeData}
            />
            <SelectPicker
              caption={'Model'}
              items={models}
              objKey={'model'}
              cb={changeData}
            />
            <SelectPicker
              caption={'Leto'}
              items={years}
              objKey={'year'}
              cb={changeData}
            />
            <SelectPicker caption={'Tip'} items={items} />
            <SelectPicker
              caption={'Število vrat'}
              items={doors}
              objKey={'doors'}
              cb={changeData}
            />
            <SelectPicker
              caption={'Število sedežov'}
              items={seats}
              objKey={'seats'}
              cb={changeData}
            />
            <SelectPicker
              caption={'Gorivo'}
              items={fuels}
              objKey={'fuel'}
              cb={changeData}
            />
            <SelectPicker
              caption={'Povprečna poraba goriva'}
              items={averageFuelUsage}
              objKey={'fuelUsage'}
              cb={changeData}
            />
            <SelectPicker
              caption={'Menjalnik'}
              items={transmissions}
              objKey={'fuel'}
              cb={changeData}
            />
            <View style={{top: 10}}>
              <Button icon="camera" onPress={pickDocumentFromFS}>
                Dodaj dokumente vozila
              </Button>
              <View>{documents}</View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.continueContainer}>
          <Button
            mode="contained"
            onPress={() => navigation.navigate('AddPostDetails')}>
            Nadaljuj
          </Button>
        </View>
      </Background>
    </View>
  );
};

/*
        <View style={styles.continueContainer}>
          <Button
            mode="contained"
            onPress={() => navigation.navigate('AddPostDetails')}>
            Nadaljuj
          </Button>
        </View>

*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#FFF',
    width: '100%',
    alignItems: 'center',
  },
  scrollContainer: {},
  swiper: {
    backgroundColor: '#AAA',
  },
  optionsContainer: {
    width: '90%',
    left: '5%',
  },
  continueContainer: {
    height: '10%',
    justifyContent: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  selectedImagePreview: {
    position: 'absolute',
    right: 10,
    bottom: 10,
  },
  closeImage: {
    position: 'absolute',
    right: 10,
    top: 10,
  },
  cameraIcon: {
    position: 'absolute',
    right: 70,
  },
  photoIcon: {
    position: 'absolute',
    right: 10,
  },
});

export default CreatePostScreen;
