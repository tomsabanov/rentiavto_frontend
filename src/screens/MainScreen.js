/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, StyleSheet} from 'react-native';
import PostViewScreen from './PostViewScreen';
import DashboardScreen from './DashboardScreen';
import CreatePostScreen from './CreatePostScreen';
import ProfileViewScreen from './ProfileViewScreen';
import MapViewScreen from './MapViewScreen';
import ImagePickerExample from './ImagePickerExample';

import {
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import {
  useTheme,
  Avatar,
  Title,
  Drawer,
  Caption,
  Paragraph,
  Text,
  TouchableRipple,
  Switch,
  IconButton,
} from 'react-native-paper';
import HistoryScreen from './HistoryScreen';

const MainDrawer = createDrawerNavigator();
const DrawerContent = ({navigation}) => {
  return (
    <DrawerContentScrollView>
      <View style={styles.drawerContent}>
        <View style={styles.userInfoSection}>
          <Avatar.Image
            source={{
              uri:
                'https://pbs.twimg.com/profile_images/952545910990495744/b59hSXUd_400x400.jpg',
            }}
            size={50}
          />
          <Title style={styles.title}>Dawid Urbaniak</Title>
          <Caption style={styles.caption}>@trensik</Caption>
          <View style={styles.row}>
            <View style={styles.section}>
              <Paragraph style={[styles.paragraph, styles.caption]}>
                202
              </Paragraph>
              <Caption style={styles.caption}>Following</Caption>
            </View>
            <View style={styles.section}>
              <Paragraph style={[styles.paragraph, styles.caption]}>
                159
              </Paragraph>
              <Caption style={styles.caption}>Followers</Caption>
            </View>
          </View>
        </View>
        <Drawer.Section style={styles.drawerSection}>
          <DrawerItem
            icon={({color, size}) => (
              <IconButton icon="card-outline" color={color} size={size} />
            )}
            label="Ustvari objavo"
            onPress={() => navigation.navigate('CreatePost')}
          />
          <DrawerItem
            icon={({color, size}) => (
              <IconButton icon="history" color={color} size={size} />
            )}
            label="Zgodovina"
            onPress={() => navigation.navigate('History')}
          />
          <DrawerItem
            icon={({color, size}) => (
              <IconButton icon="account-outline" color={color} size={size} />
            )}
            label="Profile"
            onPress={() => navigation.navigate('ProfileView')}
          />
          <DrawerItem
            icon={({color, size}) => (
              <IconButton icon="tune" color={color} size={size} />
            )}
            label="Preferences"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({color, size}) => (
              <IconButton icon="bookmark-outline" color={color} size={size} />
            )}
            label="Bookmarks"
            onPress={() => {}}
          />
        </Drawer.Section>
        <Drawer.Section title="Preferences">
          <TouchableRipple onPress={() => {}}>
            <View style={styles.preference}>
              <Text>Dark Theme</Text>
              <View pointerEvents="none">
                <Switch value={false} />
              </View>
            </View>
          </TouchableRipple>
          <TouchableRipple onPress={() => {}}>
            <View style={styles.preference}>
              <Text>RTL</Text>
              <View pointerEvents="none">
                <Switch value={false} />
              </View>
            </View>
          </TouchableRipple>
        </Drawer.Section>
      </View>
    </DrawerContentScrollView>
  );
};

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    marginTop: 20,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});

const MainScreen = ({navigation}) => (
  <MainDrawer.Navigator
    initialRouteName="Dashboard"
    drawerContent={DrawerContent}>
    <MainDrawer.Screen name="Dashboard" component={DashboardScreen} />
    <MainDrawer.Screen name="PostView" component={PostViewScreen} />
    <MainDrawer.Screen name="CreatePost" component={CreatePostScreen} />
    <MainDrawer.Screen name="History" component={HistoryScreen} />
    <MainDrawer.Screen name="ProfileView" component={ProfileViewScreen} />
    <MainDrawer.Screen name="MapView" component={MapViewScreen} />
    <MainDrawer.Screen name="ImagePicker" component={ImagePickerExample} />
  </MainDrawer.Navigator>
);

export default MainScreen;
