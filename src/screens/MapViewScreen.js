import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';

import {deviceHeight, deviceWidth} from '../core/constants';
import MapView from 'react-native-maps';

const MapViewScreen = ({navigation}) => {
  const region = {
    latitude: 46.056946,
    longitude: 14.505751,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  };

  return (
    <View style={styles.container}>
      <MapView style={styles.map} region={region} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: deviceWidth,
    height: deviceHeight,
  },
});

export default MapViewScreen;
