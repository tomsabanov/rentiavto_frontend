import React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {Button} from 'react-native-paper';

import Background from '../components/Background';
import SelectPicker from '../components/SelectPicker';
import {getNumbers} from '../core/utils';

const AddPostDetailsScreen = ({navigation}) => {
  const pricePerDay = getNumbers(1, 500);
  const addHours = getNumbers(1, 100);
  const addkm = getNumbers(0, 10, 0.1);
  const ageLimit = getNumbers(18, 25);
  const insurance = [{label: 'Zavarovanje Triglav', value: 'Triglav'}];

  pricePerDay.forEach((item) => (item.label += ' €'));
  addHours.forEach((item) => (item.label += ' €'));
  addkm.forEach((item) => (item.label += ' €'));

  const data = {
    pricePerDay: null,
    addHour: null,
    addKm: null,
    ageLimit: null,
    insurance: null,
  };

  const changeData = (key, value) => {
    console.log(key + ' ' + value);
    data[key] = value;
  };

  const post = () => {
    console.log(data);
  };

  return (
    <Background>
      <ScrollView style={styles.scrollContainer}>
        <View style={styles.optionsContainer}>
          <SelectPicker
            caption={'Cena na dan'}
            items={pricePerDay}
            objKey={'pricePerDay'}
            cb={changeData}
          />
          <SelectPicker
            caption={'Dodatna ura'}
            items={addHours}
            objKey={'addHour'}
            cb={changeData}
          />
          <SelectPicker
            caption={'Dodatni kilometer'}
            items={addkm}
            objKey={'addKm'}
            cb={changeData}
          />
          <SelectPicker
            caption={'Limita starosti'}
            items={ageLimit}
            objKey={'ageLimit'}
            cb={changeData}
          />
          <SelectPicker
            caption={'Zavarovanje'}
            items={insurance}
            objKey={'insurance'}
            cb={changeData}
          />
        </View>
      </ScrollView>
      <View style={styles.continueContainer}>
        <Button mode="contained" onPress={post}>
          Objavi oglas
        </Button>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    height: '100%',
    width: '100%',
    top: '15%',
  },
  optionsContainer: {
    width: '90%',
    left: '5%',
  },
  continueContainer: {
    height: '10%',
    justifyContent: 'center',
  },
});

export default AddPostDetailsScreen;
