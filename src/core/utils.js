export const getNumbers = (from, to, by = 1) => {
  let items = [];
  for (let i = from; i <= to; i = i + by) {
    items.push({
      label: round(i, 1) + '',
      value: round(i, 1),
    });
  }
  return items;
};

export const round = (value, precision) => {
  const multiplier = Math.pow(10, precision || 0);
  return Math.round(value * multiplier) / multiplier;
};
