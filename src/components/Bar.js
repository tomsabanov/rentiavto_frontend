import React from 'react';
import {StyleSheet} from 'react-native';
import {theme} from '../core/theme';
import {Appbar, Searchbar} from 'react-native-paper';

const Bar = ({openDrawer}) => {
  const [searchQuery, setSearchQuery] = React.useState('');
  const onChangeSearch = (query) => setSearchQuery(query);

  const _handleMore = () => console.log('15');

  return (
    <Appbar.Header style={styles.header} statusBarHeight={30}>
      <Appbar.Action
        icon="menu"
        color="gray"
        size={30}
        onPress={openDrawer}
        style={styles.icon}
      />
      <Searchbar
        placeholder="Search"
        onChangeText={onChangeSearch}
        value={searchQuery}
        style={styles.searchBar}
      />
      <Appbar.Action
        icon="bell-outline"
        color="gray"
        size={30}
        onPress={_handleMore}
        style={styles.icon}
      />
    </Appbar.Header>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#FFF',
  },
  searchBar: {
    width: '70%',
    height: '75%',
  },
  icon: {
    left: '2%',
  },
});

export default Bar;
