import React, {useState} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import {Caption} from 'react-native-paper';

const SelectPicker = ({caption, items, objKey, cb}) => {
  const [option, setOption] = useState(undefined);

  let itemsList = items.map((item) => {
    return (
      <Picker.Item label={item.label} value={item.value} key={item.value} />
    );
  });

  return (
    <View style={styles.container}>
      <Caption style={styles.caption}>{caption}</Caption>
      <Picker
        selectedValue={option}
        onValueChange={(itemValue, itemIndex) => {
          setOption(itemValue);
          cb(objKey, itemValue);
        }}
        dropdownIconColor={'#000'}
        prompt={'Izberi vrednost'}>
        {itemsList}
      </Picker>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderBottomColor: '#A9A9A9',
    borderBottomWidth: 1,
  },
  caption: {
    top: 10,
  },
});

export default SelectPicker;
