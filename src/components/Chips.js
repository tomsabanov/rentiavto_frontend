import React from 'react';
import {StyleSheet, FlatList} from 'react-native';
import {Chip} from 'react-native-paper';

const ChipItem = ({title, filter, removeFilter}) => (
  <Chip
    style={styles.chip}
    onClose={removeFilter}
    onPress={() => console.log('Pressed')}>
    {title}
  </Chip>
);

const Chips = ({filters, removeFilter}) => {
  const renderItem = ({item}) => (
    <ChipItem title={item.title} removeFilter={() => removeFilter(item.id)} />
  );

  return (
    <FlatList
      data={filters}
      renderItem={renderItem}
      keyExtractor={(item) => item.id}
      horizontal={true}
      showsHorizontalScrollIndicator={false}
    />
  );
};

const styles = StyleSheet.create({
  chip: {
    marginLeft: 15,
  },
});

export default Chips;
