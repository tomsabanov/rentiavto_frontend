import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  ImageBackground,
} from 'react-native';
import {Camera as ExpoCamera} from 'expo-camera';
import {IconButton} from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';

const Camera = ({visible, closeCamera}) => {
  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(ExpoCamera.Constants.Type.back);
  useEffect(() => {
    (async () => {
      const {status} = await ExpoCamera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  let camera = null;
  const [spinner, setSpinner] = useState(false);
  const [images, setImages] = useState([]);
  const addImage = (im) => {
    setImages([...images, im.uri]);
  };
  const removeImage = (photo) => {
    let newImages = [];
    for (let im of images) {
      if (im == photo) {
        continue;
      }
      newImages.push(im);
    }
    setImages(newImages);
  };

  const takeAPicture = async () => {
    if (camera) {
      setSpinner(true);
      let photo = await camera.takePictureAsync();
      addImage(photo);
      setSpinner(false);
      console.log(photo);
    }
  };

  if (hasPermission === null || hasPermission === false) {
    return null;
  }

  if (!visible) {
    return null;
  }

  return (
    <View style={styles.container}>
      <Spinner
        visible={spinner}
        textContent={'Shranjujem sliko'}
        textStyle={styles.spinnerTextStyle}
      />
      <ExpoCamera
        style={styles.camera}
        type={type}
        ref={(ref) => {
          camera = ref;
        }}>
        <View style={styles.imagesList}>
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={images}
            renderItem={({item, index}) => (
              <ImageBackground
                source={{uri: item}}
                key={index}
                style={styles.image}>
                <IconButton
                  icon={'close'}
                  size={15}
                  color="white"
                  style={styles.selectedImagePreview}
                  onPress={() => removeImage(item)}
                />
              </ImageBackground>
            )}
          />
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => {
              setType(
                type === ExpoCamera.Constants.Type.back
                  ? ExpoCamera.Constants.Type.front
                  : ExpoCamera.Constants.Type.back,
              );
            }}>
            <Text style={styles.text}> Flip </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={takeAPicture}>
            <Text style={styles.text}> Take A Picture </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => closeCamera(images)}>
            <Text style={styles.text}> OK </Text>
          </TouchableOpacity>
        </View>
      </ExpoCamera>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  camera: {
    flex: 1,
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 0,
    width: '100%',

    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
  text: {
    fontSize: 18,
    color: 'white',
    margin: 30,
  },
  imagesList: {
    width: '100%',
  },
  image: {
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: '#d35647',
    resizeMode: 'contain',
    top: 30,
    margin: 5,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
});

export default Camera;
