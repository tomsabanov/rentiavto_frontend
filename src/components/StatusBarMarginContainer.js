import React from 'react';
import {ImageBackground, StyleSheet, StatusBar, View} from 'react-native';
import {theme} from '../core/theme';

const StatusBarMarginContainer = ({children}) => (
  <ImageBackground
    source={require('../assets/background_dot.png')}
    resizeMode="repeat"
    style={styles.background}>
    <View style={styles.container} behavior="height">
      {children}
    </View>
  </ImageBackground>
);

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: theme.colors.surface,
  },
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
  },
});

export default StatusBarMarginContainer;
