import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {theme} from '../core/theme';
import {FlatGrid} from 'react-native-super-grid';
import {Button, Card, Avatar, IconButton, Surface} from 'react-native-paper';

import {deviceHeight, deviceWidth} from '../core/constants';

const Grid = ({props, navigation}) => {
  const [items, setItems] = React.useState([
    {name: 'TURQUOISE', code: '#1abc9c'},
    {name: 'EMERALD', code: '#2ecc71'},
    {name: 'PETER RIVER', code: '#3498db'},
    {name: 'AMETHYST', code: '#9b59b6'},
    {name: 'WET ASPHALT', code: '#34495e'},
    {name: 'GREEN SEA', code: '#16a085'},
    {name: 'NEPHRITIS', code: '#27ae60'},
    {name: 'BELIZE HOLE', code: '#2980b9'},
    {name: 'WISTERIA', code: '#8e44ad'},
    {name: 'MIDNIGHT BLUE', code: '#2c3e50'},
    {name: 'SUN FLOWER', code: '#f1c40f'},
    {name: 'CARROT', code: '#e67e22'},
    {name: 'ALIZARIN', code: '#e74c3c'},
    {name: 'CLOUDS', code: '#ecf0f1'},
    {name: 'CONCRETE', code: '#95a5a6'},
    {name: 'ORANGE', code: '#f39c12'},
    {name: 'PUMPKIN', code: '#d35400'},
    {name: 'POMEGRANATE', code: '#c0392b'},
    {name: 'SILVER', code: '#bdc3c7'},
    {name: 'ASBESTOS', code: '#7f8c8d'},
  ]);

  return (
    <FlatGrid
      itemDimension={deviceWidth / 3 - 10}
      data={items}
      style={styles.gridView}
      spacing={10}
      renderItem={({item}) => (
        <Surface style={styles.surface}>
          <Card elevation={10} onPress={navigation.navigate('PostView')}>
            <Card.Cover
              source={{uri: 'https://picsum.photos/700'}}
              style={styles.card}
            />
            <Card.Title
              title="Ime Avta "
              titleStyle={styles.name}
              style={styles.info}
              right={(props) => <Text>1000$</Text>}
            />
          </Card>
        </Surface>
      )}
    />
  );
};

const styles = StyleSheet.create({
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  surface: {
    elevation: 4,
  },
  card: {
    height: deviceHeight / 6,
  },
  info: {
    height: deviceHeight / 18,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
  },
  name: {
    fontSize: 15,
  },
});
export default Grid;
