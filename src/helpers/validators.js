export function passwordValidator(password) {
  if (!password || password.length <= 0) {
    return "Password can't be empty.";
  }
  return '';
}

export function nameValidator(name) {
  if (!name || name.length <= 0) {
    return "Name can't be empty.";
  }
  return '';
}

export function emailValidator(email) {
  const re = /\S+@\S+\.\S+/;
  if (!email || email.length <= 0) {
    return "Email can't be empty.";
  }
  if (!re.test(email)) {
    return 'Ooops! We need a valid email address.';
  }
  return '';
}
